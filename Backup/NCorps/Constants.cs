﻿/*
 * Créé par Benjamin Vella
 * Date: 20/03/2014
 * Heure: 08:28
 * 
 * 
 */
using System;

namespace NCorps
{
	/// <summary>
	/// Description of Constants.
	/// </summary>
	public class Constants
	{
		// En secondes
		public const float refreshTime = 0.5f;
		
		// Constante gravitationnelle, SI
		public const double g = 6.67384e-11;
		
	}
}
