﻿/*
 * Créé par Benjamin Vella
 * Date: 18/03/2014
 * Heure: 15:28
 * 
 * 
 */
using System;

namespace NCorps
{
	/// <summary>
	/// Description of Corps.
	/// </summary>
	public class Corps
	{
		public Position Pos {get; set;}
		public Vitesse Vit {get; set;}
		public int Masse {get; set;}
		
		public Corps(Position p, Vitesse v, int m)
		{
			this.Pos = p;
			this.Vit = v;
			this.Masse = m;
		}
		public Corps(Position p, Vitesse v) : this(p,v,0) {}
		public Corps(Position p) : this(p,new Vitesse()){}
		public Corps() : this(new Position()){}
		
		public override String ToString()
		{
			return "Corps de masse " + this.Masse + " en position " + this.Pos.ToString() + " a la vitesse de " + this.Vit.ToString();
		}
		
	}
}
