﻿/*
 * Créé par Benjamin Vella
 * Date: 18/03/2014
 * Heure: 15:36
 * 
 * 
 */
using System;
using System.Collections.Generic;
using System.Timers;

namespace NCorps
{
	/// <summary>
	/// Description of Engine.
	/// </summary>
	public class Engine
	{
		public List<Corps> World {get; set;}
		
		public Engine(int n)
		{
			this.World = new List<Corps>();
			for(int i = 1; i <= n ; i++)
			{
				this.World.Add(new Corps());
			}
		}
		public Engine() : this(2) {}
		
		public void Run()
		{
			//Compte a rebours sur Constants.refreshTime
			//On lance Instant() quand le compte a rebours atteind 0
		}
		
		private void Instant()
		{
			System.Console.WriteLine("Actualisation des postions...");
			Vitesse();
			Deplacement();
			System.Console.WriteLine(this);
		}
		
		private void Vitesse()
		{
			foreach(Corps c in this.World)
				foreach(Corps c2 in this.World)
					if(! c.Equals(c2))
						c.Vit = Calcul(c,c2);
		}
		
		private void Deplacement()
		{
			foreach(Corps c in this.World)
			{
				c.Pos.x += c.Vit.Vx*Constants.refreshTime ;
				c.Pos.y += c.Vit.Vy*Constants.refreshTime;
			}
		}
		
		private Vitesse Calcul(Corps c, Corps c2)
		{
			Vitesse v = new Vitesse();
			
			//On calcule la distance entre les 2 points
			double d = Distance(c.Pos,c2.Pos);
			
			// Et l'angle par rapport a l'axe (Ox)
			double alpha = Math.Acos((c2.Pos.x - c.Pos.x)/d);
			
			// Pour pouvoir calculer l'acceleration
			double acceleration = Constants.g * c.Masse * c2.Masse / (d*d);
			
			// On la decoupe en fonction des directions.
			double ax, ay;
			ax = acceleration * Math.Cos(alpha);
			ay = acceleration * Math.Sin(alpha);
			
			// Et on en déduit les vitesses en x et y.
			v.Vx = ax*Constants.refreshTime + c.Vit.Vx;
			v.Vy = ay*Constants.refreshTime + c.Vit.Vy;
			
			return v;
		}
		
		public static double Distance(Position p1, Position p2)
		{
			return Math.Sqrt((double)(p2.x - p1.x) + (double)(p2.y - p1.y));
		}
		
		public override String ToString()
		{
			String result = "Le systeme est compose de " + this.World.Count + " corps. Voici leurs caracteristiques : \n\n";
			foreach(Corps c in this.World)
			{
				result += c.ToString() + "\n";
			}
			
			return result;
		}
	}
}
