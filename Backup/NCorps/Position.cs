﻿/*
 * Créé par Benjamin Vella
 * Date: 18/03/2014
 * Heure: 15:29
 * 
 * 
 */
using System;

namespace NCorps
{
	/// <summary>
	/// Description of Position.
	/// </summary>
	public class Position
	{
		public double x {get; set;}
		public double y {get; set;}
		
		public Position(double x, double y)
		{
			this.x = x;
			this.y = y;
		}
		
		public Position() : this(0.0,0.0){}
		
		public override String ToString()
		{
			return "(x,y) = (" + this.x + "," + this.y + ")";
		}
	}
}
