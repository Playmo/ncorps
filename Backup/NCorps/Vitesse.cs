﻿/*
 * Créé par Benjamin Vella
 * Date: 20/03/2014
 * Heure: 09:14
 * 
 * 
 */
using System;

namespace NCorps
{
	/// <summary>
	/// Description of Vitesse.
	/// </summary>
	public class Vitesse
	{
		public double Vx {get; set;}
		public double Vy {get; set;}
		
		public Vitesse(double vx, double vy)
		{
			this.Vx = vx;
			this.Vy = vy;
		}

		public Vitesse() : this(0.0,0.0) {}
		
		public override String ToString()
		{
			return "(Vx,Vy) = (" + this.Vx + "," + this.Vy + ")";
		}
	}
}
