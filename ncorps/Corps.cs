﻿/*
 * Créé par Benjamin Vella
 * Date: 18/03/2014
 * Heure: 15:28
 * 
 * 
 */
using System;
using System.Drawing;
using System.Collections.Generic;

namespace NCorps
{
	/// <summary>
	/// Description of Corps.
	/// </summary>
	public class Corps
	{
		public PointF Pos {get; set;}
		public PointF Vit {get; set;}
		public double Masse {get; set;}
        public Color Color { get; set; }
		
		public Corps(PointF p, PointF v, double m, Color c)
		{
			this.Pos = p;
			this.Vit = v;
			this.Masse = m;
            this.Color = c;
		}
        public Corps(PointF p, double m, Color c) : this(p, new PointF(), m, c) { }
       
        public delegate void ValueHaveChangedHandler(PointF value, Color c);
        public event ValueHaveChangedHandler ValueHasChanged;

        private void fireValueHaveChanged(PointF value, Color c)
        {
            // if anyone has subscribed, notify them
            if (ValueHasChanged != null)
            {
                ValueHasChanged(value,c);
            }
        }

        public void CalculsWithAllCorps(List<Corps> corps)
        {
            foreach (Corps c in corps)
            {
                if (!this.Equals(c))
                {
                    this.Calcul(c);
                }
            }       
        }

        public double DistanceTo(Corps c2)
        {
            double d1 = Math.Pow((c2.Pos.X - this.Pos.X),2);
            double d2 = Math.Pow((c2.Pos.Y - this.Pos.Y), 2);
			return Math.Sqrt(d1 + d2);
        }
        public void Deplacement()
        {
            this.Pos = new PointF(this.Pos.X + (float)this.Vit.X * Constants.refreshTime, this.Pos.Y + (float)this.Vit.Y * Constants.refreshTime);
            fireValueHaveChanged(this.Pos,this.Color);
        }

        public void Calcul(Corps c2)
        {
            //On calcule la distance entre les 2 points
            double d = this.DistanceTo(c2);

            // Pour pouvoir calculer l'acceleration
            double acceleration = (Constants.g * this.Masse * c2.Masse / (d * d)) / this.Masse;

            // Et l'angle (positif, verification necessaire) par rapport a l'axe (Ox)
            double alpha = Math.Acos((c2.Pos.X - this.Pos.X) / d);

            // On la decoupe en fonction des directions.
            double ax, ay;
            ax = acceleration * Math.Cos(alpha);
            ay = acceleration * Math.Sin(alpha);
            if (this.Pos.Y > c2.Pos.Y) ay = -1.0 * ay;

            // Et on en déduit les vitesses en x et y.
            PointF newVit = new PointF((float)(ax * Constants.refreshTime + this.Vit.X), (float)(ay * Constants.refreshTime + this.Vit.Y));
            this.Vit = newVit;
        }

		public override String ToString()
		{
			return "Corps de masse " + this.Masse + " en position " + this.Pos.ToString() + " a la vitesse de " + this.Vit.ToString();
		}
		
	}
}
