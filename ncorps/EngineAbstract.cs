﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace NCorps
{
    public abstract class EngineAbstract
    {
        public List<Corps> World { get; set; }

        public EngineAbstract()
        {
            this.World = new List<Corps>{
                new Corps(new PointF(300.0f, 300.0f), 60000000000,Color.Red),
                new Corps(new PointF(350.0f, 400.0f), 50000000000, Color.Blue),
                new Corps(new PointF(421.0f, 320.0f), 60000000000, Color.Green)
            };

        }

        public override String ToString()
        {
            String result = "Le systeme est compose de " + this.World.Count + " corps. Voici leurs caracteristiques : \n\n";
            foreach (Corps c in this.World)
            {
                result += c.ToString() + "\n";
            }

            return result;
        }

        public abstract void Run();
    }
}
