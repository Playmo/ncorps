﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NCorps
{
    class EngineConcurrency : EngineAbstract
    {
        public EngineConcurrency() : base() { }

        public override void Run()
        {
            do{
                //on va calculer pour chaque corps sa prochaine position en parallele
                Parallel.ForEach(World, corps =>
                {
                    Console.WriteLine("calcul of corps masse #{0}...", corps.Masse);
                    corps.CalculsWithAllCorps(this.World);
                });

                //on va en deplacer chaque corps en parallele
                Parallel.ForEach(World, corps => corps.Deplacement() );

                //on recommence
            } while (true);
        }
    }
}
