﻿/*
 * Créé par Benjamin Vella
 * Date: 18/03/2014
 * Heure: 15:36
 * 
 * 
 */
using System;
using System.Collections.Generic;
using System.Timers;
using System.Drawing;

namespace NCorps
{
    /// <summary>
    /// Description of Engine.
    /// </summary>
    public class EngineNoConcurrency : EngineAbstract
    {
        public EngineNoConcurrency() : base() { }

        public override void Run()
		{
            do
            {
                //Calcul des vitesses
                World.ForEach(c => c.CalculsWithAllCorps(World));

                //Mise a jour des deplacements
                World.ForEach(c => c.Deplacement());        

                System.Console.WriteLine(this);
            } while (true);
		}
    }
}
