﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NCorps
{
    public partial class FormAnimationCorps : Form
    {
        private EngineAbstract engine = new EngineConcurrency();

        public FormAnimationCorps()
        {
            InitializeComponent();
        }

        private void FormAnimationCorps_Shown(object sender, EventArgs e)
        {
            engine.World.ForEach(corps => corps.ValueHasChanged += c_ValueHasChanged);
            engine.Run();
        }

        void c_ValueHasChanged(PointF value, Color c)
        {
            try
            {
                Graphics formGraphics = this.CreateGraphics();
                SolidBrush brush = new SolidBrush(c);

                Rectangle r = new Rectangle(new Point((int)value.X, (int)value.Y), new Size(4, 4));
                formGraphics.FillEllipse(brush, r);

                brush.Dispose();
                formGraphics.Dispose();
            }
            catch (Exception) { };
        }

    }
}
