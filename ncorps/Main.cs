﻿/*
 * Créé par Benjamin Vella
 * Date: 18/03/2014
 * Heure: 19:37
 * 
 * 
 */
using System;
using System.Windows.Forms;

namespace NCorps
{
	/// <summary>
	/// Description of Main.
	/// </summary>
	public class Go
	{
		
		static void Main(String[] args)
		{
            /*
			System.Console.WriteLine("Creation du systeme");
			
			Engine system = new Engine();
			
			System.Console.WriteLine("Lancement...");
			
			system.Run();
			
			System.Console.ReadLine();
		
             */
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FormAnimationCorps());
          }
	}
}
